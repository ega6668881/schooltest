import logo from './logo.svg';
import './App.css';
import Header from './components/header/header'
import Stats from './components/stats/table'
import Playground from './components/playground/playground';
import Login from './components/auth/login/login'
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import ActivePlayers from './components/activePlayers/activePlayers'
import CheckAuth from './components/auth/checkAuth/checkAuthComponent';
import { Provider, observer } from 'mobx-react';
import ComponentWebSocket from './websocket/websocket';
import { wss } from './websocket/websocketConfig';
import { useEffect, useRef } from 'react';
import HistoryGames from './components/historyGames/historyGames';


function App({ClientStore}) {
  console.log(ClientStore)
  window.addEventListener("beforeunload", function(e) {
      console.log("Неактивен")
      wss.send(JSON.stringify({type: "changeStatus", idUser: ClientStore.idUser, status: false}))
  })
  return (
    <body>
      <Provider value={ClientStore}>
      <ComponentWebSocket clientStore={ClientStore}/>
        <Header activeButton="stats" clientStore={ClientStore}/>
        <Routes>
          <Route path="/" element={<Stats clientStore={ClientStore}/>}/>
          <Route path="historyGames" element={<CheckAuth clientStore={ClientStore} Component={<HistoryGames clientStore={ClientStore}/>}/>} />
          <Route path="playground" element={<CheckAuth clientStore={ClientStore} Component={<Playground clientStore={ClientStore}/>}/>} />
          <Route path="activePlayers" element={<CheckAuth clientStore={ClientStore} Component={<ActivePlayers clientStore={ClientStore}/>}/>} />
          <Route path='auth/login' element={<Login clientStore={ClientStore}/>} />
        </Routes>
    </Provider>
    </body>
  );
}

export default observer(App);
