import logo from "../media/logo.svg"
import { Link, Navigate, BrowserRouter, NavLink, useNavigate } from 'react-router-dom';
import singout from "../media/Vectorsinout.svg"
import "./header.css"
import React, { useState } from "react";
import Nofication from "../modals/notification/nofication";
import { observer } from "mobx-react";
import { wss } from "../../websocket/websocketConfig";

function Header ({clientStore}) {
    const navigate = useNavigate()
    const ws = wss
    const activeButton = clientStore.activeButton
    const [inviteAuthorId, setInviteAuthorId] = useState(null)
    const [inviteAccept, setInviteAccept] = useState(null)
    ws.addEventListener('message', event => {
        const message = JSON.parse(event.data)
        switch(message.type) {
            case "invite": {
                if (message.idUser == clientStore.idUser){
                    clientStore.invited = true
                    setInviteAuthorId(message.idAuthor)
                }
                break
            }
            case "inviteAccept": {
                console.log(message)
                if (message.idUser == clientStore.idUser) {
                    clientStore.isGame = true
                    navigate('/playground')
                }
                break
            }
                
        }
    })
    const setInvited = () => {
        clientStore.invited = false
    }

    const createGame = () => {
        clientStore.invited = false
        fetch('http://89.23.98.53:3000/api/v1/playground', {
            method: "POST",
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({idPlayer1: clientStore.idUser, idPlayer2: inviteAuthorId})
        }).then(response => {
            response.json().then(result => {
                clientStore.isGame = true
                
                ws.send(JSON.stringify({type: "inviteAccept", idUser: inviteAuthorId}))
                navigate('/playground')
            })
        })
    }

    const logout = () => {
        localStorage.removeItem('token')
        ws.send(JSON.stringify({type: "changeStatus", idUser: clientStore.idUser, status: false}))
        clientStore.setLogout()
        navigate('/')
        
    }
    return (
        <header>
            <Nofication active={clientStore.invited} setActive={setInvited}>
                <div className='text'>Вам пришел инвайт!</div>
                <div className="button-yes" onClick={() => createGame()}>Принять</div>
                <div className="button-no">Отклонить</div>
            </Nofication>
            <div id = "logo"><img src={logo} alt="test"></img></div>
                <div id = "nav-bar">
                    <Link to="/playground"><div className={activeButton == 'playground' ? 'active': 'nav'}>Игровое поле</div></Link>
                    <Link to='/'><div className={activeButton == 'stats' ? 'active': 'nav'}>Рейтинг</div></Link>
                    <Link to="/activePlayers"><div className={activeButton == 'activePlayers' ? 'active': 'nav'}>Активные игроки</div></Link>
                    <Link to="/historyGames"><div className={activeButton == 'historyGames' ? 'active': 'nav'}>История игр</div></Link>
                    <div class = "nav">Список игроков</div>
                </div>
            {clientStore.isLogin ? (<div className = 'singout' onClick={logout}>{clientStore.fullName} Выход</div>) : (<div id = "singout"><a href="/auth/login"><img src={singout} alt="test"></img></a></div>)}
        </header>
    );
}

export default observer(Header);