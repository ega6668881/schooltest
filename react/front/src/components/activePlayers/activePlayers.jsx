
import { useEffect, useState } from "react"
import Header from "../header/header"
import './style.css'
import { observer } from "mobx-react"
import { wss } from "../../websocket/websocketConfig"
import Nofication from "../modals/notification/nofication"

function ActivePlayers({clientStore}) {
    const [rows, setRows] = useState("Загрузка")
    const ws = wss
    clientStore.activeButton = 'activePlayers'
    console.log(clientStore)
    const createGame = (id) => {

    }

    
    useEffect(() => {
        fetch('http://89.23.98.53:3000/api/v1/users/getUsers', {
            method: "GET",
            headers: {'Content-Type': 'application/json'}
        })
        .then(response => {
            if (response.status == 200) {
                let tempRows = []
                console.log(response.json().then((result => {
                    result.users.forEach(user => {
                        console.log(user.id != clientStore.idUser)
                        if (user.id != clientStore.idUser){
                            tempRows.push(<tr class="table-row">
                            <th class="name"><div>{user.surName} {user.firstName} {user.secondName}</div></th>
                            <th class="field">{user.statusGame ? (<div className="status-busy">В игре</div>) : (<div className="status-free">Свободен</div>)}</th>
                            <th class="field">{user.statusGame ? (<div className="invite-false">Позвать</div>) : (<div className="invite-true" onClick={() => clientStore.sendInvite(user.id)}>Позвать</div>)}</th>
                            </tr>)
                        }
                        
                    setRows(tempRows)
                    })
                })))
            }
        })
        }, [])

    return (
       <div>
            <div class="active-players">
                <table>
                    <caption class="title"><h1 class="title">Активные игроки</h1></caption>Только свободные<label className='switch'><input type="checkbox" /><span className="slider"></span></label>
                        {rows}
                </table>
            </div>
        </div>
    )
}



export default observer(ActivePlayers)