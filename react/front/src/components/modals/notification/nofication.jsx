import React from 'react'
import './modal.css'

const Nofication = ({active, setActive, children}) => {
    if (active){
        const timeout = setTimeout(() => {
            setActive(false)
            clearTimeout(timeout)
        }, 5000)
    }
    return (
        <div className={active ? "nofication nofactive" : "nofication"} onClick={() => setActive(false)}>
            <div className='nofication-content' onClick={e => e.stopPropagation()}>
                {children}
            </div>
        </div>
        
    )
}

export default Nofication