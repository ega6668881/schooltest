import "./playground.css"
import union from "../media/Union.svg"
import zero from "../media/zero.svg"
import vectorsend from "../media/Vectorsend.svg"
import React from 'react';
import Fields from './fields/fields'
import GameStore from "../../stores/components/playground/GameStore";
import store, { StoreContext, useStore } from '../../stores/components/playground/store'
import { Provider } from "mobx-react";
import Header from "../header/header";
import Timer from "./timer/timer";
import { observer } from "mobx-react";
import { wss } from "../../websocket/websocketConfig";
import { useNavigate } from "react-router-dom";

function Playground({clientStore}) {
    const navigate = useNavigate()
    clientStore.activeButton = 'playground'
    const gameStore = useStore().GameStore
    clientStore.isFinishedGame = false
    gameStore.clientUserId = clientStore.idUser
    gameStore.getGame(clientStore)
    const ws = wss
    ws.addEventListener('message', event => {
        const message = JSON.parse(event.data)
        switch(message.type){
            case "fillCellValue": {
                if (message.idUser == clientStore.idUser) {
                    gameStore.getGame(clientStore)
                    
                }
            }
        }
    })
    if (!clientStore.isGame) return (<div>Ошибка! Вы не в игре!</div>)
    return (
        <div>
            <Timer Store={gameStore} />
            <div class = "players">
            <div class = "players-title"><h2 class="title">Игроки</h2></div>
            <div class = "player"><img src={union} alt="fsd"></img> {gameStore.firstPlayerName}</div>
            <div class = "player"><img src={zero} alt="fgdfg"></img> {gameStore.secondPlayerName}</div>
            </div>
            <Provider store={store}>
                <Fields GameStore={gameStore} clientStore={clientStore}/>
            </Provider>
            <div class="chat">
                <div class="message-box-1">
                    <div class="title">Плюшкина Екатерина 
                        <span class="message-time">13:40</span>
                        
                    </div>
                    <div class="message">Ну что, готовься к поражению!!1</div>
                </div>
                <div class="message-box-2">
                    <div class="title">Пупкин Владлен
                        <span class="message-time">13:41</span>
                        
                    </div>
                    <div class="message">Надо было играть за крестики. Розовый - мой не самый счастливый цвет</div>
                </div>
                <div class="message-box-2">
                    <div class="title">Пупкин Владлен
                        <span class="message-time">13:45</span>
                        
                    </div>
                    <div class="message">Я туплю...</div>
                </div>
                <div class="message-box-1">
                    <div class="title">Плюшкина Екатерина 
                        <span class="message-time">13:47</span>
                        
                    </div>
                    <div class="message">Отойду пока кофе попить, напиши в тг как сходишь</div>
                </div>
                <div class="message-send">
                    Сообщение...
                    <div class="send"><img src={vectorsend} alt="fsdf"></img></div>
                </div>
            </div>
            
        </div>
    );
}

export default observer(Playground);