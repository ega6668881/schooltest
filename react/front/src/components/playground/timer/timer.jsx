import { useState, useEffect } from 'react';
import './timer.css'
import { observer } from "mobx-react";
import Modal from "../../modals/finishedGame/Modal"
import winImage from '../../media/winner.svg'

function Timer({Store}) {
    const [modalActive, setModalActive] = useState(false) 
    const getTime = () => {
        Store.updateTimer()
        if (Store.timer <= 0) {
            //Store.stopGame()
            return setModalActive(true)
        }
        
    }
    useEffect(() => {
        if (!Store.isFinishedGame) {
            const interval = setInterval(() => getTime(), 1000)
            
            return () => clearInterval(interval)
        }
        
    })

    return (<div>
            <div class="timer">{Math.floor(Store.timer/60)}:{Store.timer-(Math.floor((Store.timer/60))*60)}</div>
            <Modal active={modalActive} setActive={setModalActive}>
                <div className='winner'>Ничья!</div>
                <div className='new-game' onClick={() => Store.NewGame()}>Новая игра</div>
                <div className='exit-to-menu'>Выйти в меню</div>
            </Modal>
        </div>)
}

export default observer(Timer)