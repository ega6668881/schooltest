import "./fields.css"
import React from 'react';
import { useState, useEffect, localStorage } from 'react';
import { observer } from "mobx-react";
import Modal from '../../modals/finishedGame/Modal'
import Nofication from '../../modals/notification/nofication'
import gameStore from "../../../stores/components/playground/GameStore";
import { useStore } from "../../../stores/components/playground/store";
import Timer from "../timer/timer";
import winImage from '../../media/winner.svg'

function Fields({GameStore, clientStore}) {
    const [modalActive, setModalActive] = useState(GameStore.isFinishedGame)
    const [fieldNofication, setNoficationActive] = useState(false)
    let fields = []
    for (let row = 0; row < GameStore.fields.length; row++) {
        for (let col = 0; col < GameStore.fields[row].length; col++) {
            if (GameStore.fields[row][col]) {
                fields.push(<div><img onClick={() => setNoficationActive(true)} className="field" src={GameStore.ImageSrc(GameStore.fields[row][col])}></img></div>)
            }
            else fields.push(<div class="field" onClick={() => GameStore.FieldCellValue(row, col, clientStore)}></div>)
        }
    }
    console.log(GameStore.winner, GameStore.isFinishedGame)
    return (
        <div>
        <Nofication active={fieldNofication} setActive={setNoficationActive}><div className='text'>Клетка занята!</div></Nofication>
        <Modal active={GameStore.isFinishedGame} setActive={setModalActive}>
                <div className='image'><img src={winImage} alt='winimage' className='image'></img></div>
                <div className='winner'>{GameStore.winner ? (GameStore.winner[1]) : (null)}</div>
                <div className='new-game' onClick={() => clientStore.sendInvite(GameStore.opponentId, GameStore)}>Предложить реванш</div>
                <div className='exit-to-menu'>Выйти в меню</div>
        </Modal>
        <div class="playing-field">
            {fields}
        </div>
        <div class="mode">
            <div class="mode-text">Ходит</div>
            <div class="mode-img"><img src={GameStore.ImageSrc()} width="24px" height="24"></img></div>
            <div class="mode-player">{GameStore.playerStepName}</div>
        </div>
        
        </div>
        
    )
}

export default observer(Fields)