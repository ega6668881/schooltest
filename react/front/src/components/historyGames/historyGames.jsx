import { observer } from "mobx-react";
import Table from "../table/table";
import { useEffect } from "react";

function HistoryGames({clientStore}) {
    let fields = []
    clientStore.activeButton = "historyGames"
    useEffect(() => {
        fetch('http://89.23.98.53:3000/api/v1/playground/getGames', {
            method: "POST",
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({idUser: clientStore.idUser})
        }).then(response => {
            if (response.status == 200) {
                response.json().then(result => {
                    console.log(result.games)
                    fields.push(<tr class="table-row">
                        <th class="name"><div>Игроки</div></th>
                        <th class="field">Дата</th>
                        <th class="field"><div>Время игры</div></th>
                    </tr>)
                })
            }
        })
    })

    return (<Table caption="История игр" rows={<tr class="table-row">
    <th class="name"><div>Игроки</div></th>
    <th class="field">Дата</th>
    <th class="field"><div>Время игры</div></th>
    </tr>} content={fields}/>)

}

export default observer(HistoryGames)