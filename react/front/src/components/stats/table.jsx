import './table.css'
import { useState, useEffect } from 'react'
import mockUsersApi from "../../mockApi/mockUsersApi"
import Header from '../header/header'
import { observe } from 'mobx'
import { observer } from 'mobx-react-lite'
import Table from '../table/table'


function StatsTable({clientStore}) {
    clientStore.activeButton = 'stats'
    const [users, setUsers] = useState([])
    useEffect(() => {
        mockUsersApi.getUsers().then(result => {
            setUsers(result)
        })
    })
    let fields = []
    if (users.length > 0) {
        users.forEach(user => {mockUsersApi.getUsers()
            fields.push(<tr class="table-row">
            <td class="name">{user.name}</td>
            <td class="field">{user.games_count}</td>
            <td class="field"><strong class="wins">{user.wins}</strong></td>
            <td class="field"><strong class="loose">{user.looses}</strong></td>
            <td class="field">{user.p_winner}</td>  
        </tr>)
        })
    }
    else {
        fields = <p>Загрузка</p>
    }
    return (
        <Table caption={"Реитинг игроков"} rows={<tr class="table-row"><th class="name">ФИО</th>
        <th class="field">Всего игр</th>
        <th class="field">Победы</th>
        <th class="field">Проигрыши</th>
        <th class="field">Процент побед</th>
    </tr>} content={fields} />
        
    )
    
}

export default StatsTable