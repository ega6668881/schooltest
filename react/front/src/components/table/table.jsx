import './table.css'

function Table({rows, content, caption}) {
    return (<div class="stats-players">
                <table>
                    <caption class="title"><h1 class="title">{caption}</h1></caption>
                    {rows}
                    {content}
                </table>
            </div>)
}

export default Table