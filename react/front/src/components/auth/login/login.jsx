import './login.css'
import loginDog from '../../media/login_dog.svg'
import { useEffect, useState } from 'react';
import { Navigate } from 'react-router-dom';
import { observer } from 'mobx-react';
import wss from '../../../websocket/websocketConfig'

function Login ({clientStore}) {
    const [buttonActive, setButtonActive] = useState(true)
    const [fieldsCheck, setFieldsCheck] = useState(0)
    const [errorVisible, setVisibleError] = useState(true)
    const [errorMessage, setErrorMessage] = useState(`Логин и пароль должены содержать минимум 8 симловов Допускается только латиница`)
    const [login, setLogin] = useState('')
    const [isLogin, setLoginned] = useState(false)
    const [password, setPassword] = useState('')
    const isKyr = function (str) {
        return /[а-я]/i.test(str);
    }
    useEffect(() => {

    })
    const inputCheck = (e) => {
        if (e.target.name == "login") setLogin(e.target.value)
        else setPassword(e.target.value)
        if (isKyr(e.target.value)) {
            setButtonActive(true)
            setVisibleError(true)
            setFieldsCheck(fieldsCheck - 1)
        }
        else {
            setFieldsCheck(fieldsCheck + 1)
        }

        if (fieldsCheck === 2) {
            setButtonActive(false); 
            setVisibleError(false);
            
        }
    }

    const loginCheck = (e) => {
        e.preventDefault()
        
        fetch("http://89.23.98.53:3000/api/v1/auth/login", {
            method: "POST",
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                username: login,
                password: password
            })
        }).then(response => {
            console.log(response.status == 401)
            if (response.status == 200) {
                response.json().then(result => {
                    localStorage.setItem("token", result.token)
                    console.log(result)
                    clientStore.setLogined(true, result.username, result.idUser, result.userRow)
                    setLoginned(true)
                    
                })
            } else if (response.status == 401) {
                response.json().then(result => {
                    console.log(result.message)
                    setErrorMessage(result.message)
                    setVisibleError(true)
                })
                
            }
            
        })
    }
    if (isLogin) return <Navigate to="/"/>
    return (
        <body>
            <div class="login">
                <div class="logo"><img src={loginDog}></img></div>
                <div class="title-block"><h1 class="title-text">Войдите в игру</h1></div>
                <div class="login-form">
                    <form onSubmit={loginCheck}>
                        <input placeholder="Логин" class="fields" name="login" onChange={inputCheck} onpaste="return false" onCopy="return false" onCut="return false"></input>
                        <input placeholder="Пароль" class="fields" name="password" onChange={inputCheck} onpaste="return false" onCopy="return false" onCut="return false" type="password"></input>
                        <div className={errorVisible ? "error-active" : "error"}>{errorMessage}</div>
                        <button type='submit' class="login-button" disabled={buttonActive}>Войти</button>
                    </form>
                </div>
            </div>
        </body> 
    );
}

export default observer(Login)
