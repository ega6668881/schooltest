import { observer } from "mobx-react"
import Login from "../login/login"
import { useEffect, useState } from "react"
import { wss } from "../../../websocket/websocketConfig"

function CheckAuth({clientStore, Component}) {
    const [auth, setAuth] = useState(null)
    useEffect(() => {
        fetch("http://89.23.98.53:3000/api/v1/auth/authCheck", {
        method: "POST",
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({
            "x-access-token": localStorage.getItem('token')
        })
        }).then(response => {
            console.log(response.status)
            if (response.status == 200) {
                response.json().then(result => {
                    console.log(result)
                    setAuth(true)
                    clientStore.setLogined(true, result.username, result.idUser, result.userRow, result.userRow.statusGame)
                    return wss.send(JSON.stringify({type: "changeStatus", idUser: clientStore.idUser, status: true}))
                })
                
                 
            }
            else if (response.status == 401) {
                clientStore.setLogout()
                setAuth(false)
            }
        })
    }, [])

    if (auth) {
        return (Component)

    } else if (auth === false) return (<Login clientStore={clientStore}/>) 
    else return (<div>Проверка авторизации</div>)
}

export default CheckAuth