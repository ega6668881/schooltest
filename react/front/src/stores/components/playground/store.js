import { createContext, useContext } from "react";
import GameStore from "./GameStore";

const store =  {
    GameStore: new GameStore(),
}

export const StoreContext = createContext(store);

export const useStore = () => {
  return useContext(StoreContext);
};

export default store;