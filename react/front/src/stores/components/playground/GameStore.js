import { action, makeAutoObservable, observable } from "mobx";
import zero from '../../../components/playground/fields/media/zero_playing.svg'
import vector from "../../../components/playground/fields/media/Vector_playing.svg" 
import mockGameApi from "../../../mockApi/mockGameApi"
import DataBase from "../../../mockApi/DB";
import { useEffect } from "react";
import { wss } from "../../../websocket/websocketConfig";


class GameStore {
  fields = [
    [null, null, null],
    [null, null, null],
    [null, null, null]
  ]
  gameId = null
  timer = null
  opponentId = null
  opponentName = null
  clientUserId = null
  firstPlayerId = null
  playerStepName = null
  secondPlayerId = null
  firstPlayerName = null
  secondPlayerName = null
  winner = null
  updateTimer = action(() => {
    this.timer -= 1
  })
  setPlayersNames = action((newFirstPlayerName, newSecondPlayerName) => {
    this.firstPlayerName = newFirstPlayerName
    this.secondPlayerName = newSecondPlayerName
  })
  isStep = false
  mode = null
  gameMode = null
  isFinishedGame = false
  StrConvert = {
    "x": "Крестики",
    "o": "Нолики"
  }
  stopGame() {
    if (this.isFinishedGame){
      fetch('http://89.23.98.53:3000/api/v1/playground/stopGame', {
            method: "PUT",
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({gameId: this.gameId, winner: this.winner[0]})
      }).then(response => {})
    }
    
  }
  getGame(clientStore){
    fetch('http://89.23.98.53:3000/api/v1/playground/getGame', {
            method: "POST",
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({idUser: this.clientUserId})
    }).then(response => {
      if (response.status == 200) {
        if (this.isFinishedGame) this.isFinishedGame = false
        this.winner = null
        response.json().then(result => {
          this.setPlayersNames(`${result.gamePlayers[0].surName} ${result.gamePlayers[0].firstName} ${result.gamePlayers[0].secondName}`,
            `${result.gamePlayers[1].surName} ${result.gamePlayers[1].firstName} ${result.gamePlayers[1].secondName}`)
          if (this.clientUserId == result.game.idPlayer1) {
            this.mode = 'x' 
            this.opponentId = result.game.idPlayer2
            this.opponentName = this.secondPlayerName
          } else {
            this.mode = 'o' 
            this.opponentId = result.game.idPlayer1
            this.opponentName = this.firstPlayerName
          }
          
          if (this.clientUserId == result.game.isStep) {
            this.isStep = true
            this.gameMode = this.mode
          } else if (this.mode == "x") {
            this.gameMode = "o"
          } else this.gameMode = "x"
          this.fields = result.game.playground
          let different = new Date() - new Date(result.game.gameBegin)
          this.timer = 10000 - Math.floor(different/1000)
          this.firstPlayerId = result.game.idPlayer1
          this.secondPlayerId = result.game.idPlayer2
          this.gameId = result.game.id
          if (this.timer <= 0) this.timer = 0
          console.log(this.opponentName, clientStore.fullName)
          if (this.checkGame(this.mode) == true){
              this.winner = [clientStore.idUser, clientStore.fullName]
          } else if (this.mode == "x") {
            if (this.checkGame("o") == true)
              this.winner = [this.opponentId, this.opponentName]
          } else if (this.mode == "o") {
            if (this.checkGame("x") == true) {
              this.winner = [this.opponentId, this.opponentName]
            }
          }
          else {
              return false
          }
          let nullCheck = 0
          this.fields.forEach(row => {
            row.forEach(col => {
              if (col === null) nullCheck += 1
            })
          })
          

          if (this.isStep) this.playerStepName = clientStore.fullName
          else this.playerStepName = this.opponentName

          if (this.winner != null && this.winner != "Ничья"){
              this.isFinishedGame = true
              this.stopGame()
          } 
          if (nullCheck == 0) {
            this.winner = [null, "Ничья!"]  
            this.isFinishedGame = true
            this.stopGame()
          }
          
        })
      }
      
    })
  }
  constructor (){
    makeAutoObservable(this)
  }
  ImageSrc(symbol = this.gameMode){
    if (symbol === "x"){
        return vector
         
    }
    else {
        return zero
    }
  }
  FieldCellValue(y, x, clientStore) {
    if (this.isStep && !this.isFinishedGame) {
      if(this.fields[y][x]) {
        return false
      }
      else {
          this.isStep = false
          this.fields[y][x] = this.mode
          wss.send(JSON.stringify({type: 'fillCellValue', gameId: this.gameId, playground: this.fields}))
          this.getGame(clientStore)
      }
    }
    }
    
  checkGame(symbol){
    let combinations = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6]
    ]
    return combinations.some((comb) =>
        comb.every((elem) => {
            const row = Math.floor(elem / 3)
            const col = elem % 3
            
            return this.fields[row][col] === symbol
            
          })
      )
  }
  NewGame(){
    mockGameApi.deleteField(1)
    this.mode = "x"
    this.fields = [
      [null, null, null],
      [null, null, null],
      [null, null, null]
    ]
    this.isFinishedGame = false
  }
}


export default GameStore;