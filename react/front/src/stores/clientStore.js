import { makeAutoObservable, observable, action } from "mobx";
import { createContext, useContext } from "react";
import { wss } from "../websocket/websocketConfig";

class ClientStore {
    username = null
    idUser = null
    invited = false
    isLogin = false
    isGame = false
    status = false
    fullName = null
    activeButton = ''
    setLogined = action((newLogin = true, newUsername, newIdUser, userRow, newIsGame = false) => {
        console.log(userRow)
        this.isLogin = newLogin
        this.username = newUsername
        this.idUser = newIdUser
        this.fullName = `${userRow.surName} ${userRow.firstName} ${userRow.secondName}`
        this.status = true
        this.isGame = newIsGame
    })
    setLogout = action(() => {
        this.isLogin = false
        this.username = null
        this.idUser = null
    })
    constructor () {
        makeAutoObservable(this)
    }
    sendInvite = action((id, GameStore = null) => {
        if (GameStore) GameStore.isFinishedGame = false
        fetch('http://89.23.98.53:3000/api/v1/users/sendInvite', {
            method: "POST",
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({idUser: id, idAuthor: this.idUser})
        }).then(response => {
            response.json().then(result => {
                console.log(result)
            })
        })
    })
}


const clientStore =  {
    ClientStore: new ClientStore(),
}

export const StoreContext = createContext(clientStore);

export const useStore = () => {
  return useContext(StoreContext);
};

export default ClientStore;