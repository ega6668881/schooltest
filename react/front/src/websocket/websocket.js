import { observer } from "mobx-react";
import { useEffect, useRef } from "react";
import {wss} from './websocketConfig'

function ComponentWebSocket({clientStore}) {
    const ws = useRef(null)
    useEffect(() => {
        if (!ws.current) {
            ws.current = wss;
            ws.current.addEventListener('message', event => {
              const message = JSON.parse(event.data);
              if (localStorage.getItem('token')) {
                    switch(message.type) {
                        case "checkAuth": {
                            if (message.success){
                                console.log(message.username, message.userRow)
                                clientStore.setLogined(true, message.username, message.idUser, message.userRow, false)
                                ws.current.send(JSON.stringify({type: "changeStatus", idUser: message.idUser, status: true}))
                            } else if (message.success == false){
                                clientStore.setLogout()
                                localStorage.removeItem('token')
                                ws.current.send(JSON.stringify({type: "changeStatus", idUser: message.idUser, status: false}))
                            }
                        }
                        
                    }
                }
            })
              
        }
    }, [])

    useEffect(() => {
        if (ws.current.readyState === 1){
            const token = localStorage.getItem("token")
            console.log(token)
            if (token) {
                console.log(token)
                ws.current.send(JSON.stringify({type: "checkAuth", token: token}))
            } else console.log("LOGOUT"); clientStore.setLogout()
        const interval = setInterval(() => {
                const token = localStorage.getItem("token")
                if (token) {
                    ws.current.send(JSON.stringify({type: "checkAuth", token: token}))
                } else clientStore.setLogout()
        }, 1800*1000)
        return () => {
            console.log(1321312)
        }
    }
        
        
    })


}


export default ComponentWebSocket