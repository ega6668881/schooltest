import { json } from "react-router-dom"
import IndexedDbRepository from "./DB"
import mockApiCall from "./mockApiCall"
import fields from "../components/playground/fields/fields"

const SessionPlayers = [{
    id: 0,
    login: "ega6668881",
    password: "$2b$10$02162dhx1x4LTMYydOgtP.jnLJD/CY2KUWhFuCrk5UMieD3f4b2GW",
    wins: 100,
    games_count: 150,
    looses: 50,
    p_winner: "56%",
    name: "Лукьянов Егор Вячеславович"
},
{
    id: 1,
    login: "ega",
    password: "$2b$10$02162dhx1x4LTMYydOgtP.jnLJD/CY2KUWhFuCrk5UMieD3f4b2GW",
    wins: 75,
    games_count: 50,
    looses: 50,
    p_winner: "15%",
    name: "Пупкин Василий Пупкович"
}
]

let GameChat = []

const mockGameApi = {
    createField: async (item) => {
        const repository = new IndexedDbRepository('id')
        const result = await repository.save(item)
        return result
    },
    updateTimer: async (time) => {
        localStorage.setItem("timer", time)
    },
    getPlayersInfo: () => mockApiCall(SessionPlayers),
    sendMessage: ({userId, text}) => {
    },
    mesageSend: ({userId, text}) => {
        
    },
    moveFailed: (errorMessage) => {
        return console.log(errorMessage)
    },
    makeMove: async (id, row, col, mode) => {
        let newMode = "x"
        if (mode === "x") newMode = "o"
        const repository = new IndexedDbRepository('id')
        const fields = await repository.findById(id)
        if (fields.fields[row][col]) return mockGameApi.moveFailed("Клетка занята!")
        else {
            fields.fields[row][col] = mode
            fields['mode'] = newMode
            console.log("fdsfsfd1", fields.fields)
            const result = await repository.save(fields)
            return result
        }
        
        
    },
    deleteField: async (id) => {
        const repository = new IndexedDbRepository('id')
        const result = await repository.deleteById(id)
        return result
    },
    getField: async ({id}) => {
        const repository = new IndexedDbRepository('id')
        const fields = await repository.findById(1)
        try {
            return fields
        } 
        catch {
            return null
        }
        
    }

}

export default mockGameApi