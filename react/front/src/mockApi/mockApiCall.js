const mockApiCall = (data) =>
    new Promise((resolve, reject) => {
        setTimeout(() => {
            if (Math.random() < 0.1) {
                reject(new Error());
            } else {
                resolve(data);
            }
        }, Math.random() * 10000);
    });



export default mockApiCall
