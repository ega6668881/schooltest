import mockApiCall from './mockApiCall'

const users = [{
    login: "ega6668881",
    password: "$2b$10$02162dhx1x4LTMYydOgtP.jnLJD/CY2KUWhFuCrk5UMieD3f4b2GW",
    wins: 100,
    games_count: 150,
    looses: 50,
    p_winner: "56%",
    name: "Лукьянов Егор Вячеславович"
},
{
    login: "ega",
    password: "$2b$10$02162dhx1x4LTMYydOgtP.jnLJD/CY2KUWhFuCrk5UMieD3f4b2GW",
    wins: 75,
    games_count: 50,
    looses: 50,
    p_winner: "15%",
    name: "Пупкин Василий Пупкович"
}
]

const mockUsersApi = {
    getUsers: () => mockApiCall(users),
    addUser: ({name}) => {
        return mockApiCall(undefined).then(() => users.push(users.at(-1).name = name))
    }
}

export default mockUsersApi