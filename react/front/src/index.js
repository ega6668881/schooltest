import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import reportWebVitals from './reportWebVitals';
import { Provider, observer } from 'mobx-react';
import ClientStore from './stores/clientStore';
const root = ReactDOM.createRoot(document.getElementById('root'));
const clientStore = new ClientStore()
root.render(
  <>
    <React.StrictMode>
      <BrowserRouter>
        <Provider><App ClientStore={clientStore}/></Provider>
      </BrowserRouter>
    </React.StrictMode>
  </>
);

reportWebVitals();
