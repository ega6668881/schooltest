import express from 'express'
import jwt from 'jsonwebtoken'
import dotenv from 'dotenv'
import Knex from 'knex'
import knexfile from '../db/knexfile.cjs'
import bcrypt, { hashSync } from 'bcrypt'

dotenv.config()
const router = express.Router();
const knex = Knex(knexfile.development)

const comparePassword = (password, hashedPassword) =>
    bcrypt.compareSync(password, hashedPassword);

function generateAccessToken(username) {
    return jwt.sign(username, process.env.TOKEN_SECRET, { expiresIn: '1800s' });
}

router.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});


router.post('/login', (req, res) => {
    if (!req.body.username | !req.body.password) {
        return res.status(405).json({error: "Параметры запроса не соответствуют схеме"})
    } else {
        knex.select(['idUser', 'password']).from('accounts').where('login', req.body.username)
            .then(row => {
                row = row[0]
                if (comparePassword(req.body.password, row.password)) {
                    knex('users').update({"statusActive": true}).where("id", row.idUser)
                    .returning('*')
                    .then(result => {
                        return res.status(200).json({'token': generateAccessToken({username: req.body.username, idUser: row.idUser}),
                        username: req.body.username, idUser: row.idUser, userRow: result[0]})
                    })
                    
                }
                else return res.status(401).json({message: "Неправильный пароль"})
            })
            .catch(err => {
                console.log(err)
                return res.status(401).json({message: "Пользователь не найден"})
            })
    }
    
})

router.post('/register', (req, res) => {
    const keys = ["username", "password", "surName", "firstName", "secondName", "gender", "age"]
    if (!(Object.keys(req.body).length === keys.length && Object.keys(req.body).every((el, idx) => el === keys[idx]))) {
        return res.status(405).json({error: "Параметры запроса не соответствуют схеме"})
    } else {
        knex.select(['idUser', 'password']).from('accounts').where('login', req.body.username)
            .then(row => {
                if (row.length > 0) return res.status(401).json({error: "Пользователь уже существует."})
                else {
                    knex('users').insert({"surName": req.body.surName,
                        "firstName": req.body.firstName,
                        "secondName": req.body.secondName,
                        "gender": req.body.gender,
                        "statusActive": false,
                        "statusGame": false,
                        "age": req.body.age,
                        "createdAt": new Date(),
                        "updateAt": new Date()}).returning('*')
                    .then(result => {
                        knex('accounts').insert({"idUser": result[0].id, "login": req.body.username, "password": hashSync(req.body.password, 10)})
                            .then(result => {
                                let accessToken = generateAccessToken({username: req.body.username})
                                return res.status(200).json({token: accessToken})
                            })
                        
                    })
                    .catch(err => {
                        console.log(err)
                    })
                }
                
            })
            .catch(err => {
                console.log(err)
            })
        
    }
})

router.post('/authCheck', (req, res) => {
    const token = req.body['x-access-token'];
    if (!token) {
        res.status(401).json({ message: 'Токен не найден' });
    } else {
        jwt.verify(token, process.env.TOKEN_SECRET, function(err, decoded) {
        if (err) {
            res.status(401).json({ message: 'Недействительный токен' });
        } else {
            knex('accounts').select("login").where({idUser: decoded.idUser})
            .then(rows => {
                knex('users').select("*").where({id: decoded.idUser})
                .then(userRows => {
                    res.status(200).json({ message: 'Доступ разрешен', idUser: decoded.idUser, username: rows[0].login, userRow: userRows[0] });
                })
                
            })
        }
        });
    }
})

export default router