import express from 'express'
import Knex from 'knex'
import knexfile from '../db/knexfile.cjs'
const knex = Knex(knexfile.development)

const router = express.Router();
let playgrounds = []

router.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});


router.post('/getGames', (req, res) => {
    if (Object.keys(req.body).length === 0) {
        res.status(400).json({error: "Сервер не может обработать пустой запрос"})

    } else if ("idUser" in req.body){
        knex('games').select("*").where(function() {this.where({idPlayer1: req.body.idUser}).orWhere({idPlayer2: req.body.idUser}).whereNotNull('gameEnd')})
        .then(result => {
            const test = (games) => new Promise((resolve, reject) => {
                for(let i; i <= games.length; i++) {
                    console.log(i)
                    knex('users').select("surName", "firstName", "secondName").where({"id": games[i].idPlayer1})
                    .then(users => {
                        console.log("FSDFSFDF")
                        games[i][namePlayer1] = `${users[0].surName} ${Array.from(users[0].firstName)[0]}.${Array.from(users[0].secondName)[0]}.`
                        knex('users').select("surName", "firstName", "secondName").where({"id": games[i].idPlayer1})
                        .then(users => {
                            games[i][namePlayer2] = `${users[0].surName} ${Array.from(users[0].firstName)[0]}.${Array.from(users[0].secondName)[0]}.`
                            resolve(games)
                        })
                        
                    })
                }
            }).then(result => {
                console.log("result")
                res.status(200).json({games: result, success: true})
            })
            test(result)  

            
        })

    } else res.status(405).json({error: "Параметры запроса не соответствуют схеме"})
    
})

router.post('/', (req, res) => {
    if (Object.keys(req.body).length === 0) {
        res.status(400).json({error: "Сервер не может обработать пустой запрос"})

    } else if ("idPlayer1" in req.body && "idPlayer2" in req.body){
        const playground = [
            [null, null, null],
            [null, null, null],
            [null, null, null]
        ]
        console.log(req.body)
        knex('games').insert({idPlayer1: req.body.idPlayer1,
            idPlayer2: req.body.idPlayer2,
            gameBegin: new Date(),
            playground: playground,
            isStep: req.body.idPlayer1})
        .then(result => {
            knex('users').whereIn("id", [req.body.idPlayer1, req.body.idPlayer2]).update({statusGame: true})
            .then(result => {
                res.status(200).json({message: "Игра начата"})
            })
           
        })
        
    } else res.status(405).json({error: "Параметры запроса не соответствуют схеме"})
    
})

router.post('/getGame', (req, res) => {
    if (Object.keys(req.body).length == 0) res.status(400).json({error: "Сервер не может обработать пустой запрос"})
    else if ("idUser" in req.body){
        knex('games').select("*").where(function() {this.where({idPlayer1: req.body.idUser, gameEnd: null}).orWhere({idPlayer2: req.body.idUser, gameEnd: null})})
        .then(result => {
            knex('users').select(["id", "firstName", "surName", "secondName"]).where(function() {
                this.where("id", result[0].idPlayer1).orWhere("id", result[0].idPlayer2)
            }).then(users => {
                let gamePlayers = []
                if (users[0].id == result[0].idPlayer1){
                    gamePlayers = [users[0], users[1]]
                } else gamePlayers = [users[1], users[0]]
                
                res.status(200).json({"success": true, game: result[0], gamePlayers: gamePlayers})
            })
            .catch(err => {})
            if (!result) res.status(404).json({error: "Игра"})
        })

        
        
    }    
    else res.status(405).json({error: "Параметры запроса не соответствуют схеме"})
})

router.put('/stopGame', (req, res) => {
    console.log(req.body)
    if ("gameId" in req.body && 'winner' in req.body){
        knex('games').update({gameEnd: true, gameResult: req.body.winner, gameEnd: new Date()}).where({id: req.body.gameId})
        .returning('*')
        .then(result => {
            knex('users').whereIn("id", [result[0].idPlayer1, result[0].idPlayer2]).update({statusGame: false})
            .then(result => {
                res.status(200).json({success: true})
            })
            
        })
        
    } else {
        res.status(405).json({error: "Параметры запроса не соответствуют схеме"})
    }
})

router.put('/', (req, res) => {
    if (Object.keys(req.body).length == 0) res.status(400).json({error: "Сервер не может обработать пустой запрос"})   
    else if ("gameId" in req.body && "playground" in req.body){
        let fieldCheck = false
        playgrounds.forEach(field => {
            if (field.id == req.body.id){
                fieldCheck = true
                console.log(field)
                playgrounds[playgrounds.indexOf(field)].playground = req.body.playground 
                console.log(playgrounds[playgrounds.indexOf(field)].playground )
                res.status(200).json({message: `Игровое поле под индексом ${field.id} изменено!`})
            }
        })
        if (!fieldCheck) res.status(404).json({error: "Игровое поле не найдено"})
        
    } 
    else res.status(405).json({error: "Параметры запроса не соответствуют схеме"})
})

export default router