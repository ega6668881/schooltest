import express from 'express'
import users from '../users.json' assert {type: "json"}
import bcrypt from 'bcrypt'
import fs from 'fs'
import Knex from 'knex'
import knexfile from '../db/knexfile.cjs'
import WebSocket from 'ws'
import {wss} from '../index.js'


const router = express.Router();
const knex = Knex(knexfile.development)


const userBodyCheck = ["id", "wins", "looses", "firstName", "lastName", "password", "games_count", "login"]
const saltRounds = 10;
const salt = bcrypt.genSaltSync(saltRounds);


router.post('/getUser', (req, res) => {
    if (Object.keys(req.body).length == 0){
        res.status(400).json({error: "Сервер не может обработать пустой запрос"})
    }
    else if ("idUser" in req.body) {
       knex('users').select(["surName", "firstName", "lastName"]).where({"id": req.body.idUser})
    }
    else res.status(405).json({error: "Параметры запроса не соответствуют схеме"})
})

router.get('/getUsers', (req, res) => {
    knex.select(['statusActive', 'statusGame', 'firstName', 'surName', 'secondName', "id"]).from('users').where({"statusActive": true}).then(result => {
        return res.status(200).json({users: result})
    })
    
})

router.post('/sendInvite', (req, res) => {
    if ('idUser' in req.body && 'idAuthor' in req.body) {
        knex('users').select(['firstName', 'surName', 'secondName']).where({id: req.body.idUser})
        .then(result => {
            res.status(200).json({"success": true, user: result[0]})
            wss.clients.forEach(client => {
                if (client.readyState == WebSocket.OPEN) {
                    client.send(JSON.stringify({"type": "invite", "idUser": req.body.idUser, "idAuthor": req.body.idAuthor}))
                }
            })
        })
        
        

    } else return res.status(405).json({error: "Параметры запроса не соответствуют схеме"})
})

router.post('/', (req, res) => {
    
    if (Object.keys(req.body).length == 0){
        res.status(400).json({error: "Сервер не может обработать пустой запрос"})
    }
    else {
        let checker = false
        Object.keys(req.body).forEach(key => {
            if (!key in userBodyCheck) checker = true
        })
        if (checker) res.status(405).json({error: "Параметры запроса не соответствуют схеме"})
        req.body.password = bcrypt.hashSync(req.body.password, salt)
        req.body['p_winner'] = `${Math.round(req.body.wins/req.body.games_count*100)}%`
        users.push(req.body)
        console.log(req.body)
        fs.writeFileSync('./users.json', JSON.stringify(users), 'utf-8')
        res.status(200).json({message: "Пользователь добавлен"})
    }
})


export default router