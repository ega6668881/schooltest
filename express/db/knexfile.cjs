// Update with your config settings.

/**
 * @type { Object.<string, import("knex").Knex.Config> }
 */
module.exports = {

  development: {
    client: 'pg',
    connection: {
      host: "89.23.98.53",
      user: 'postgres',
      password: '3251',
      port: 5432,
      database: 'db_xoxo_dev',
      
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      directory: "./migrations"
    }
  },
};
