
exports.up = function(knex) {
    return knex.schema.createTable('games', function(table) {
        table.increments('id').primary()
        table.integer('idPlayer1').unsigned()
        table.foreign('idPlayer1').references('users.id').onDelete('cascade');
        table.integer('idPlayer2').unsigned()
        table.foreign('idPlayer2').references('users.id').onDelete('cascade');
        table.boolean("gameResult")
        table.timestamp('gameBegin').notNullable()
        table.timestamp('gameEnd').notNullable()
        // table.string('playground')
        table.specificType('playground', 'text[]')
        table.integer('isStep')
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('games')
};
