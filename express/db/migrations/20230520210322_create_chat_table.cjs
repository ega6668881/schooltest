
exports.up = function(knex) {
    return knex.schema.createTable('messenger', function(table) {
        table.integer('idGame').unsigned()
        table.foreign('idGame').references('games.id').onDelete('cascade');
        table.integer('idUser').unsigned()
        table.foreign('idUser').references('users.id').onDelete('cascade');
        table.text('message').notNullable()
        table.timestamp('createdAt').notNullable()
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('games')
};
