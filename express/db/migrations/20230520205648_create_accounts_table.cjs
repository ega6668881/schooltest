
exports.up = function(knex) {
    return knex.schema.createTable('accounts', function(table) {
        table.integer('idUser').unsigned()
        table.foreign('idUser').references('users.id').onDelete('cascade');
        table.string("login").notNullable()
        table.string('password').notNullable()
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('accounts')
};
