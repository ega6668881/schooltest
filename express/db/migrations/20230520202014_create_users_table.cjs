
exports.up = function(knex) {
    return knex.schema.createTable('users', function(table) {
        table.increments('id').primary()
        table.string("surName").notNullable()
        table.string("firstName").notNullable()
        table.string("secondName").notNullable()
        table.boolean("gender")
        table.boolean("statusActive")
        table.boolean("statusGame")
        table.timestamp('createdAt').notNullable()
        table.timestamp('updateAt').notNullable()
        table.integer('age').notNullable()
    })
};

exports.down = function(knex) {
  return knex.schema.dropTable('users')
};
