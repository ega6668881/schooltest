
import bcrypt from 'bcrypt'
import {WebSocketServer} from 'ws'
import http from 'http'

import bodyParser from 'body-parser'
import playgroundRoutes from "./routes/playground.js"
import usersRoutes from './routes/users.js'
import knexfile from './db/knexfile.cjs'
import authentication from './routes/authentication.js'
import express from 'express'
import jwt from 'jsonwebtoken'
import dotenv from 'dotenv'
import Knex from 'knex'

const knex = Knex(knexfile.development)
dotenv.config()
const app = express()
app.use(bodyParser.json())
const urlencodedParser = express.urlencoded({extended: false});
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

//console.log(bcrypt.hashSync('3251', 10))

app.use('/api/v1/playground', playgroundRoutes)
app.use('/api/v1/users', usersRoutes)
app.use('/api/v1/auth', authentication)

const server = http.createServer(app)
const wss = new WebSocketServer({server})

wss.on('connection', (ws) => {
    console.log('WebSocket connection established');
  
    ws.on('message', (message) => {
      
      try {
        message = JSON.parse(message)
      } catch {console.log(message, "Fsdfsfsf")}
      switch(message.type) {
        case "checkAuth": {
          jwt.verify(message.token, process.env.TOKEN_SECRET, function(err, decoded) {
            if (err) {
                ws.send(JSON.stringify({type: "checkAuth", success: false}))
            } else {
                knex('users').select("*").where({"id": decoded.idUser})
                .then(rows => {
                  ws.send(JSON.stringify({type: "checkAuth", success: true, idUser: decoded.idUser, username: decoded.username, userRow: rows[0]}))
                })
                
            }
            });
          break
        }
        case "changeStatus": {
          knex('users').where({id: message.idUser}).update({statusActive: message.status})
          .returning('*')
          .then((rows) => {
            console.log(rows)
          })
          .catch(err => {
          })
          break
        }
        case "inviteAccept": {
            wss.clients.forEach(client => {
              if (client.readyState == ws.OPEN) {
                  client.send(JSON.stringify(message))
              }
            })
          }
          
        case "fillCellValue": {
          console.log(message, "fdssf")
          knex('games').select('*').where("id", message.gameId)
          .then(rows => {
            let isPlayerStep = null
            if (rows[0].idPlayer1 == rows[0].isStep) isPlayerStep = rows[0].idPlayer2
            else isPlayerStep = rows[0].idPlayer1
            console.log(isPlayerStep)
            knex('games').update({isStep: isPlayerStep, playground: message.playground}).where("id", message.gameId)
            .returning('*')
            .then(result => {
              wss.clients.forEach(client => {
                if (client.readyState == ws.OPEN) {
                    client.send(JSON.stringify({type: 'fillCellValue', success: true, idUser: isPlayerStep}))
                }
              })
            })
            
          })
          .catch(err => {})
        }
      }
      
    });
  
    ws.on('close', () => {
      console.log('WebSocket connection closed');
    });
  });

  
server.listen(8080, () => {
    console.log(`Server started on port ${server.address().port}`);
}); 



app.listen(3000, () => {
  
})

export {wss}