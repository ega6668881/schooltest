const gameField = [
    ['o', null, 'o'],
    ['x', null, 'o'],
    ['o', 'o', 'x']
    ]; //базовое условие

const StrConvert = {
    "x": "Крестики",
    "o": "Нолики"
}
function winLog(winner) {
    return console.log(`${StrConvert[winner]} выйграли!`)
}
function checkGame(Field = gameField){ //опциональное условие игрового поля
    if (Field.every((val, i, arr) => val[0] === arr[0][0])){
        return winLog(Field[0][0])
    }
    else if (Field.every((val, i, arr) => val[1] === arr[0][1])){
        return winLog(Field[0][1])
    }
    else if (Field.every((val, i, arr) => val[2] === arr[0][2])){
       return winLog(Field[0][2])
    }
    else if (Field[0][0] === Field[1][1] && Field[1][1] === Field[2][2]){
        return winLog(Field[0][2])
    }
    else if (Field[0][2] === Field[1][1] && Field[0][2] === Field[2][0]){
        return winLog(Field[0][2])
    }
    else {
        for (let rowIndex = 0; rowIndex < Field.length; rowIndex++) {
            if (Field[rowIndex].every((val, i, arr) => val === arr[0])){
                return winLog(Field[rowIndex][0])
            }
        }
    }
    return console.log("Ничья!")
}