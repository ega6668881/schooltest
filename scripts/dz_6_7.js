class GameField {
    constructor() {
        this.StrConvert = {
            "x": "Крестики",
            "o": "Нолики"
        }
        this.state = [
            [null, null, null],
            [null, null, null],
            [null, null, null]
        ]
        this.mode = "x"
        this.isFinishedGame = false
    }

    getStateString() {
        let returnString = "\n"
        this.state.forEach(element => {
            element.forEach(field => {
                returnString += `${field} `
                
            })
            returnString += "\n"
        });
        return returnString
    }

    getGameFieldStatus() {
        return `Текущее игровое поле ${this.getStateString()}`
    }

    checkGame(symbol){
        let combinations = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6]
        ]
        return combinations.some((comb) =>
            comb.every((elem) => {
                const row = Math.floor(elem / 3)
                const col = elem % 3
                
                return this.state[row][col] === symbol
                
            })
            )
    }
    get winner() {
        console.log(this.checkGame("x"))
        console.log(this.checkGame("o"))
        let winner = null
        if (this.checkGame("x") == true){
            winner = "x"
        }
        else if (this.checkGame("o") == true) {
            winner = "o"
        }
        else {
            let nullCheck = 0
            this.state.forEach(row => {
                row.forEach(col => {
                    console.log(col === null, "fdsfsfds")
                    if (col === null) {
                        nullCheck += 1
                    } 
                })
            })
            if (nullCheck > 0) return false
            else winner = "Ничья"

        }
        if (winner != null && winner != "Ничья"){
            this.isFinishedGame = true
            return alert(`${this.StrConvert[winner]} выйграли!`)
        }
        else if (winner == "Ничья") {
            this.isFinishedGame = true
            return alert("Ничья!")
        }
        
    }

    setMode(mode) {
        this.mode = mode
        return this.mode
    }
    ImageSrc(){
        if (this.mode === "x"){
            return "./media/Vector_playing.svg"
             
        }
        else {
            return "./media/zero_playing.svg"
             
        }
    }
    FieldCellValue(x, y) {
        /*2 строки сделаны чтобы вводить координаты не с 0, а с 1,
        для удобства пользователя*/
        x -= 1
        y -= 1
        if(this.state[y][x]) {
            return alert(`Ошибка! Клетка заполнена ${this.state[y][x]}`)
        }
        else {
            //console.log("Игровое поле изменено!")
            this.state[y][x] = this.mode
            if (this.mode == "x") this.mode = "o"
            else this.mode = "x"
            this.getGameFieldStatus()
        }

    }
}

const gameField = new GameField()

let finishStep = false
document.addEventListener("click", function(e) {
    if (e.target.classList.contains('field')) {
        e.target.innerHTML = `<img src=${gameField.ImageSrc()}></img>`
        let step = e.target.id.split(" ")
        gameField.FieldCellValue(step[1], step[0])
        console.log(gameField.getGameFieldStatus())
        let elementMode = document.getElementsByClassName("mode-img")
        elementMode[0].innerHTML = `<img src=${gameField.ImageSrc()} width="24px" height="24"></img>`
        gameField.winner
    }
});
let elementMode = document.getElementsByClassName("mode-img")
elementMode[0].innerHTML = `<img src=${gameField.ImageSrc()} width="24px" height="24"></img>`
console.log("123213")
