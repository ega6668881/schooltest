var isKyr = function (str) {
    return /[а-я]/i.test(str);
}
fieldsCheck = {
    "info-login": null,
    "info-password": null,
}
document.addEventListener("input", function(e) {
    console.log(e.target)
    if (e.target.classList.contains("fields")) {
        let infoMessage = document.getElementsByClassName(e.target.name)
        console.log(!isKyr(e.target.value))
        console.log(e.target.value)
        if (isKyr(e.target.value) || e.target.value.length < 8) {
            if (infoMessage.length == 0){
                infoMessage = document.createElement('div')
                infoMessage.className = e.target.name
                infoMessage.innerHTML = `В поля можно вводить только английские<br/>буквы, цифры и знаки: точка,<br/>нижнее подчеркивание<br/>Длина не менее 8 символов`
                e.target.after(infoMessage)
                let loginButton = document.getElementsByClassName("login-button")
                loginButton[0].setAttribute("disabled", 'true')
                fieldsCheck[e.target.name] = null
            }
        }
        else {
            if (infoMessage.length != 0){
                infoMessage[0].remove()
                fieldsCheck[e.target.name] = true
            }
        }
        
        if (fieldsCheck['info-login'] && fieldsCheck['info-password']){
            let loginButton = document.getElementsByClassName("login-button")
            loginButton[0].removeAttribute("disabled")
        }
        
    }
})
