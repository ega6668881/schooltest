--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.25
-- Dumped by pg_dump version 15rc2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

--
-- Name: accounts; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.accounts (
    "idUser" integer,
    login character varying(255) NOT NULL,
    password character varying(255) NOT NULL
);


ALTER TABLE public.accounts OWNER TO postgres;

--
-- Name: games; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.games (
    id integer NOT NULL,
    "idPlayer1" integer,
    "idPlayer2" integer,
    "gameResult" integer,
    "gameBegin" timestamp with time zone NOT NULL,
    "gameEnd" timestamp with time zone,
    playground text[],
    "isStep" integer
);


ALTER TABLE public.games OWNER TO postgres;

--
-- Name: games_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.games_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.games_id_seq OWNER TO postgres;

--
-- Name: games_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.games_id_seq OWNED BY public.games.id;


--
-- Name: knex_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.knex_migrations (
    id integer NOT NULL,
    name character varying(255),
    batch integer,
    migration_time timestamp with time zone
);


ALTER TABLE public.knex_migrations OWNER TO postgres;

--
-- Name: knex_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.knex_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.knex_migrations_id_seq OWNER TO postgres;

--
-- Name: knex_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.knex_migrations_id_seq OWNED BY public.knex_migrations.id;


--
-- Name: knex_migrations_lock; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.knex_migrations_lock (
    index integer NOT NULL,
    is_locked integer
);


ALTER TABLE public.knex_migrations_lock OWNER TO postgres;

--
-- Name: knex_migrations_lock_index_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.knex_migrations_lock_index_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.knex_migrations_lock_index_seq OWNER TO postgres;

--
-- Name: knex_migrations_lock_index_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.knex_migrations_lock_index_seq OWNED BY public.knex_migrations_lock.index;


--
-- Name: messenger; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.messenger (
    "idGame" integer,
    "idUser" integer,
    "gameResult" boolean,
    message text NOT NULL,
    "createdAt" timestamp with time zone NOT NULL
);


ALTER TABLE public.messenger OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id integer NOT NULL,
    "surName" character varying(255) NOT NULL,
    "firstName" character varying(255) NOT NULL,
    "secondName" character varying(255) NOT NULL,
    gender boolean NOT NULL,
    "statusActive" boolean,
    "statusGame" boolean NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updateAt" timestamp with time zone NOT NULL,
    age integer NOT NULL
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: games id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.games ALTER COLUMN id SET DEFAULT nextval('public.games_id_seq'::regclass);


--
-- Name: knex_migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.knex_migrations ALTER COLUMN id SET DEFAULT nextval('public.knex_migrations_id_seq'::regclass);


--
-- Name: knex_migrations_lock index; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.knex_migrations_lock ALTER COLUMN index SET DEFAULT nextval('public.knex_migrations_lock_index_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: accounts; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.accounts VALUES
	(4, 'luka', '$2b$10$vJEiZF/UJI4trDWU5KGskO227evTFeMrLVQHB9GREseySr4LsnLXe'),
	(13, 'luka1', '$2b$10$dR3LUjT4vwf214XSultEpugg8wWfhcFRojffwIX9oL1NppO6NmAhu');


--
-- Data for Name: games; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.games VALUES
	(62, 4, 13, 4, '2023-06-12 10:26:26.217+03', '2023-06-12 10:26:26.575+03', '{{NULL,NULL,NULL},{NULL,NULL,NULL},{NULL,NULL,NULL}}', 4),
	(57, 4, 13, 4, '2023-06-12 10:21:51.224+03', '2023-06-12 10:23:07.358+03', '{{x,NULL,NULL},{x,o,NULL},{x,o,NULL}}', 13),
	(75, 4, 13, 4, '2023-06-13 09:32:07.6+03', '2023-06-13 09:32:14.977+03', '{{x,NULL,NULL},{x,o,NULL},{x,o,NULL}}', 13),
	(73, 4, 13, 4, '2023-06-13 09:29:37.114+03', '2023-06-13 09:29:40.826+03', '{{x,NULL,NULL},{x,o,NULL},{x,o,NULL}}', 13),
	(58, 4, 13, 4, '2023-06-12 10:23:17.251+03', '2023-06-12 10:23:18.093+03', '{{NULL,NULL,NULL},{NULL,NULL,NULL},{NULL,NULL,NULL}}', 4),
	(68, 4, 13, 4, '2023-06-13 09:21:48.068+03', '2023-06-13 09:22:13.753+03', '{{x,NULL,NULL},{x,o,NULL},{x,NULL,o}}', 13),
	(63, 4, 13, 4, '2023-06-12 10:27:25.986+03', '2023-06-12 10:27:30.7+03', '{{x,NULL,NULL},{x,o,NULL},{x,o,NULL}}', 13),
	(77, 4, 13, 13, '2023-06-13 09:33:06.776+03', '2023-06-13 09:33:14.474+03', '{{x,x,NULL},{o,o,o},{x,NULL,NULL}}', 4),
	(64, 4, 13, 4, '2023-06-12 10:27:32.981+03', '2023-06-12 10:27:33.344+03', '{{NULL,NULL,NULL},{NULL,NULL,NULL},{NULL,NULL,NULL}}', 4),
	(79, 13, 4, 13, '2023-06-13 09:38:19.56+03', '2023-06-13 09:46:11.601+03', '{{x,NULL,NULL},{x,o,NULL},{x,o,NULL}}', 4),
	(71, 4, 13, 4, '2023-06-13 09:27:02.782+03', '2023-06-13 09:27:14.722+03', '{{x,NULL,NULL},{x,o,NULL},{x,o,NULL}}', 13),
	(81, 4, 13, 4, '2023-06-13 13:48:59.668+03', '2023-06-13 13:49:04.955+03', '{{x,NULL,NULL},{x,o,NULL},{x,NULL,o}}', 13),
	(55, 4, 13, 4, '2023-06-12 10:14:58.722+03', '2023-06-12 10:20:33.277+03', '{{x,x,x},{x,o,NULL},{o,o,NULL}}', 13),
	(65, 4, 13, 4, '2023-06-13 09:19:27.657+03', '2023-06-13 09:20:15.406+03', '{{x,NULL,NULL},{x,o,NULL},{x,NULL,o}}', 13),
	(66, 4, 13, 4, '2023-06-13 09:20:23.983+03', '2023-06-13 09:20:24.271+03', '{{NULL,NULL,NULL},{NULL,NULL,NULL},{NULL,NULL,NULL}}', 4),
	(59, 4, 13, 4, '2023-06-12 10:23:41.985+03', '2023-06-12 10:23:47.782+03', '{{x,NULL,NULL},{x,o,NULL},{x,o,NULL}}', 13),
	(60, 4, 13, 4, '2023-06-12 10:23:55.264+03', '2023-06-12 10:23:55.702+03', '{{x,NULL,NULL},{NULL,NULL,NULL},{NULL,NULL,NULL}}', 13),
	(69, 4, 13, 4, '2023-06-13 09:24:10.125+03', '2023-06-13 09:24:17.926+03', '{{x,NULL,NULL},{x,o,NULL},{x,o,NULL}}', 13),
	(76, 13, 4, 4, '2023-06-13 09:32:20.871+03', '2023-06-13 09:33:00.372+03', '{{x,NULL,NULL},{o,o,o},{x,x,NULL}}', 13),
	(74, 4, 13, 4, '2023-06-13 09:30:45.724+03', '2023-06-13 09:30:49.409+03', '{{x,NULL,NULL},{x,o,NULL},{x,o,NULL}}', 13),
	(56, 4, 13, 4, '2023-06-12 10:20:43.463+03', '2023-06-12 10:20:48.249+03', '{{x,NULL,NULL},{x,o,NULL},{x,o,NULL}}', 13),
	(72, 4, 13, 4, '2023-06-13 09:28:44.825+03', '2023-06-13 09:28:51.022+03', '{{x,NULL,NULL},{x,o,NULL},{x,o,NULL}}', 13),
	(67, 4, 13, 4, '2023-06-13 09:21:24.004+03', '2023-06-13 09:21:44.75+03', '{{x,NULL,NULL},{x,o,NULL},{x,NULL,o}}', 13),
	(78, 13, 4, 4, '2023-06-13 09:33:24.858+03', '2023-06-13 09:38:17.187+03', '{{x,NULL,NULL},{o,o,o},{x,x,NULL}}', 13),
	(61, 4, 13, 4, '2023-06-12 10:26:18.324+03', '2023-06-12 10:26:23.009+03', '{{x,NULL,NULL},{x,o,NULL},{x,NULL,o}}', 13),
	(70, 4, 13, 4, '2023-06-13 09:26:02.818+03', '2023-06-13 09:26:07.072+03', '{{x,NULL,NULL},{x,o,NULL},{x,o,NULL}}', 13),
	(80, 4, 13, 4, '2023-06-13 13:48:44.699+03', '2023-06-13 13:48:56.763+03', '{{x,NULL,NULL},{x,o,NULL},{x,NULL,o}}', 13),
	(82, 4, 13, 4, '2023-06-13 13:54:54.396+03', '2023-06-13 13:55:13.555+03', '{{x,NULL,NULL},{x,o,NULL},{x,NULL,o}}', 13),
	(83, 4, 13, NULL, '2023-06-13 13:55:17.381+03', NULL, '{{NULL,NULL,NULL},{NULL,NULL,NULL},{NULL,NULL,NULL}}', 4);


--
-- Data for Name: knex_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.knex_migrations VALUES
	(13, '20230608203550_add_step_in_games.cjs', 1, '2023-06-08 23:37:30.779+03');


--
-- Data for Name: knex_migrations_lock; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.knex_migrations_lock VALUES
	(2, 0);


--
-- Data for Name: messenger; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.users VALUES
	(4, 'Лукьянов', 'Егор', 'Вячеславович', true, false, true, '2023-05-26 20:42:49+03', '2023-05-26 20:42:49+03', 10),
	(13, 't', 'tt', 'ttt', true, false, true, '2023-05-26 18:43:15.79+03', '2023-05-26 18:43:15.79+03', 18);


--
-- Name: games_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.games_id_seq', 83, true);


--
-- Name: knex_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.knex_migrations_id_seq', 13, true);


--
-- Name: knex_migrations_lock_index_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.knex_migrations_lock_index_seq', 2, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 13, true);


--
-- Name: games games_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.games
    ADD CONSTRAINT games_pkey PRIMARY KEY (id);


--
-- Name: knex_migrations_lock knex_migrations_lock_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.knex_migrations_lock
    ADD CONSTRAINT knex_migrations_lock_pkey PRIMARY KEY (index);


--
-- Name: knex_migrations knex_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.knex_migrations
    ADD CONSTRAINT knex_migrations_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: accounts accounts_iduser_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.accounts
    ADD CONSTRAINT accounts_iduser_foreign FOREIGN KEY ("idUser") REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: games games_idplayer1_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.games
    ADD CONSTRAINT games_idplayer1_foreign FOREIGN KEY ("idPlayer1") REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: games games_idplayer2_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.games
    ADD CONSTRAINT games_idplayer2_foreign FOREIGN KEY ("idPlayer2") REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: messenger messenger_idgame_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.messenger
    ADD CONSTRAINT messenger_idgame_foreign FOREIGN KEY ("idGame") REFERENCES public.games(id) ON DELETE CASCADE;


--
-- Name: messenger messenger_iduser_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.messenger
    ADD CONSTRAINT messenger_iduser_foreign FOREIGN KEY ("idUser") REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE USAGE ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

